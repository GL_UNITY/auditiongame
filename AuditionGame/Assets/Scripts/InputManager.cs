﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class InputEvent : UnityEvent<int>
{
    // https://docs.unity3d.com/ScriptReference/Events.UnityEvent_1.html
}

public class InputManager : MonoBehaviour
{
    public const int PERFECT = 100;
    public const int GREAT = 80;
    public const int GOOD = 50;
    public const int FAIL = 0;

    public KeyCode[] keys = { KeyCode.UpArrow, KeyCode.DownArrow, KeyCode.LeftArrow, KeyCode.RightArrow };
    public RectTransform[] keysRequest;
    public float timeoutScale = 1f;

    [Header("callback after process request keys")]
    public InputEvent inputEvent;

    private float timeout, elapse;
    private int numKey, totalKey, correctKey;
    private int currentRequest;
    private List<KeyCode> request;

    // Start is called before the first frame update
    void Start()
    {
        currentRequest = -1;
        request = new List<KeyCode>();

        foreach (RectTransform request in keysRequest)
        {
            request.gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        /*
        // Begin debug
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (currentRequest == -1)
            {
                GenerateRequest();
            }

        }
        // End debug
        */

        if (currentRequest != -1)
        {
            ProcessKey();

            elapse += Time.deltaTime;

            if (numKey == totalKey)
            {
                // user can see the last key color
                timeout = elapse + timeoutScale / totalKey;
                numKey++;
            }

            if (elapse > timeout)
            {
                TimeOut();
            }
        }
    }

    private void ProcessKey()
    {
        if (Input.anyKeyDown)
        {
            ArrowColor[] arrows = keysRequest[currentRequest].GetComponentsInChildren<ArrowColor>();

            bool isCorrectRequest = false;
            bool isCorrectKey = false;

            foreach (KeyCode key in keys)
            {
                if (Input.GetKeyDown(key))
                {
                    isCorrectKey = true;

                    if (request[0] == key)
                    {
                        isCorrectRequest = true;
                    }
                }
            }

            if (isCorrectKey)
            {
                if (isCorrectRequest)
                {
                    correctKey++;
                    arrows[numKey].ChangeColor(Color.green);
                }
                else
                {
                    arrows[numKey].ChangeColor(Color.red);
                }

                numKey++;
                request.RemoveAt(0);
            }

        }

    }

    public void GenerateRequest()
    {
        currentRequest = UnityEngine.Random.Range(0, keysRequest.Length);
        keysRequest[currentRequest].gameObject.SetActive(true);

        // MUST enable before change key
        GenerateKeys();
    }

    private void GenerateKeys()
    {
        request.Clear();

        ArrowColor[] arrows = keysRequest[currentRequest].GetComponentsInChildren<ArrowColor>();
        timeout = arrows.Length * timeoutScale;
        totalKey = arrows.Length;
        correctKey = numKey = 0;
        elapse = 0;

        foreach (ArrowColor arrow in arrows)
        {
            int keyIndex = UnityEngine.Random.Range(0, keys.Length);
            KeyCode key = keys[keyIndex];

            arrow.ChangeArrow(key);
            arrow.ChangeColor(Color.blue);
            request.Add(key);
        }
    }

    private void TimeOut()
    {
        keysRequest[currentRequest].gameObject.SetActive(false);
        currentRequest = -1;

        ProcessCallback();
    }

    private void ProcessCallback()
    {
        if (inputEvent != null)
        {
            float percent = 100f * correctKey / totalKey;
            Debug.Log("correct key " + correctKey + " vs total key " + totalKey + " percent " + percent + "%");

            if (percent == InputManager.PERFECT)
            {
                inputEvent.Invoke(InputManager.PERFECT);
            }
            else if (percent >= InputManager.GREAT)
            {
                inputEvent.Invoke(InputManager.GREAT);
            }
            else if (percent >= InputManager.GOOD)
            {
                inputEvent.Invoke(InputManager.GOOD);
            }
            else
            {
                inputEvent.Invoke(InputManager.FAIL);
            }
        }

    }
}
