﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image), typeof(RectTransform))]
public class ArrowColor : MonoBehaviour
{
    private Image image;
    private RectTransform rectTransform;
    // Start is called before the first frame update
    void OnEnable()
    {
        rectTransform = GetComponent<RectTransform>();
        image = GetComponent<Image>();
    }

    public void ChangeColor(Color newColor)
    {
        image.color = newColor;
    }

    public void ChangeArrow(KeyCode key)
    {
        switch(key)
        {
            case KeyCode.UpArrow:
                rectTransform.rotation = Quaternion.Euler(0, 0, 90);
                break;
            case KeyCode.DownArrow:
                rectTransform.rotation = Quaternion.Euler(0, 0, -90);
                break;
            case KeyCode.LeftArrow:
                rectTransform.rotation = Quaternion.Euler(0, 0, 180);
                break;
            case KeyCode.RightArrow:
                rectTransform.rotation = Quaternion.Euler(0, 0, 0);
                break;
        }

    }
}
