﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestInput : MonoBehaviour
{
    public void OnInputCallbackText(int result)
    {
        switch(result)
        {
            case InputManager.PERFECT:
                Debug.Log("you hit PERFECT");
                break;

            case InputManager.GREAT:
                Debug.Log("you hit GREAT");
                break;

            case InputManager.GOOD:
                Debug.Log("you hit GOOD");
                break;

            case InputManager.FAIL:
                Debug.Log("you hit FAIL");
                break;
        }
    }


    public void OnInputCallbackScore(int result)
    {
        switch (result)
        {
            case InputManager.PERFECT:
                Debug.Log("+100");
                break;

            case InputManager.GREAT:
                Debug.Log("+80");
                break;

            case InputManager.GOOD:
                Debug.Log("+50");
                break;

            case InputManager.FAIL:
                Debug.Log("+0");
                break;
        }
    }
}
