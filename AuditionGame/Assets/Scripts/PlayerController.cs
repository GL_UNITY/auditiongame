﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : Singleton<PlayerController>
{
    //public Camera camera;
    //private Rigidbody  rb;
    public enum STATUS
    {
        IDLE = 1,
        DANCING = 2
    }

    public STATUS status = STATUS.IDLE;
    private Vector3 initPos;
    private Animator animator;

    //public static PlayerController _instance;

    protected PlayerController() { }

    // Start is called before the first frame update
    void Start()
    {
        status = STATUS.IDLE;
        initPos = transform.position;
        //rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        //transform.Rotate(0.0f, 180.0f, 0.0f);
        //animator.applyRootMotion = true;
       // rb.transform.LookAt(camera.transform.position);
        animator.Play("Idle");

    }

    public void PlayIdleAnim()
    {
        transform.position = initPos;
        if(!AnimatorIsPlaying("Idle"))
        {
            animator.Play("Idle");
            status = STATUS.IDLE;
        }
    }
    public void PlayDacingAnim()
    {
        status = STATUS.DANCING;
    }

    //Update is called once per frame
    void Update()
    {
        switch(status)
        {
            case STATUS.IDLE:
            break;

            case STATUS.DANCING:
            if(!AnimatorIsPlaying("Dancing1") && !AnimatorIsPlaying("Dancing2") && !AnimatorIsPlaying("Dancing3"))
            {
                int anim = Random.Range(1, 4);

                switch(anim)
                {
                    case 1:
                        animator.Play("Dancing1");
                    break;

                    case 2:
                        animator.Play("Dancing2");
                    break;

                    case 3:
                        animator.Play("Dancing3");
                    break;

                    default:
                        animator.Play("Dancing2");
                    break;
                }
            }
            break;
        }
    }

    bool AnimatorIsPlaying()
    {
        return animator.GetCurrentAnimatorStateInfo(0).normalizedTime <= 1;
    }
    bool AnimatorIsPlaying(string stateName)
    {
        return AnimatorIsPlaying() && animator.GetCurrentAnimatorStateInfo(0).IsName(stateName);
    }

}
