﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public IngameUIMgr ingameUIMgr;
    public PlayerController[] players;
    public AudioManager audioManager;
    public InputManager inputManager;
    public float generateFrequence;
    private float timer;
    private bool canGenerate;

    private void Start()
    {
        canGenerate = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (canGenerate)
        {
            timer += Time.deltaTime;

            if (timer > generateFrequence)
            {
                inputManager.GenerateRequest();
                canGenerate = false;
            }
        }
    }

    public void StartGame()
    {
        StartGenerate();

        audioManager.Play("music");
        ingameUIMgr.gameObject.SetActive(true);
    }

    private void StartGenerate()
    {
        canGenerate = true;
        timer = 0;
    }

    private void Dancing()
    {
        foreach(PlayerController player in players)
        {
            player.PlayDacingAnim();
        }
    }

    public void onKeyResult(int percent)
    {
        Debug.Log("result " + percent);

        switch(percent)
        {
            case InputManager.FAIL:
                Idle();
                break;
            default:
                Dancing();
                ingameUIMgr.IncrementScore(percent);
                break;
            
        }

        StartGenerate();
    }

    private void Idle()
    {
        foreach (PlayerController player in players)
        {
            player.PlayIdleAnim();
        }
    }

    public void onSoundEnd(float time)
    {
        SceneManager.LoadScene("EndGame");
    }
}
