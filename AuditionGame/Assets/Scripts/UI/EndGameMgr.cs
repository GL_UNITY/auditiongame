﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Events;

[System.Serializable]
public class RePlayEvent : UnityEvent<string>
{
    // https://docs.unity3d.com/ScriptReference/Events.UnityEvent_1.html
}

public class EndGameMgr : MonoBehaviour
{
    public RePlayEvent rePlayEvent;

    [Header("Score config")]
    public string scoreKey = "score";
    public TextMeshProUGUI ScoreUI;

    [Header("Perfect config")]
    public string perfectKey = "perfect";
    public TextMeshProUGUI perfect;
    public string greatKey = "great";
    public TextMeshProUGUI great;
    public string goodKey = "good";
    public TextMeshProUGUI good;
    
    // Start is called before the first frame update
    void Start()
    {
        SetScore(PlayerPrefs.GetInt(scoreKey));
        SetPerfect(PlayerPrefs.GetInt(perfectKey));
        SetGreat(PlayerPrefs.GetInt(greatKey));
        SetGood(PlayerPrefs.GetInt(goodKey));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetScore(int value)
    {
        ScoreUI.text = "SCORE: " + value;
    }
    public void SetPerfect(int value)
    {
        perfect.text = "PERFECT: " + value;
    }
    public void SetGreat(int value)
    {
        great.text = "GREAT: " + value;
    }
    public void SetGood(int value)
    {
        good.text = "GOOD: " + value;
    }
    public void Replay()
    {
        rePlayEvent.Invoke("Replay");
    }
}
