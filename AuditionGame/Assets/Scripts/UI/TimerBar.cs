﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerBar : MonoBehaviour
{
    // Start is called before the first frame update
    private Slider timerBar;

    public float speed = 0.5f;
    private float target = 0;
    private float maxTime = 0;
    private void Awake()
    {
        timerBar = gameObject.GetComponent<Slider>();
    }
    void Start()
    {
        //IncrementProgress(0.75f);
    }

    // Update is called once per frame
    void Update()
    {
        if (timerBar.value < target)
            timerBar.value += speed * Time.deltaTime;
    }
    public void SetMaxTime(float value)
    {
        maxTime = value;
    }
    public void SetCurrentTime(float value)
    {
        target = value/ maxTime;
    }
}
