﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class IngameUIMgr : MonoBehaviour
{
    public GameObject Timer;
    public TextMeshProUGUI ScoreUI;
    public GameObject perfect;
    public GameObject great;
    public GameObject good;

    private List<ParticleSystem> listParticle = new List<ParticleSystem>();

    [Header("Set time max for Game")]
    //public float MAX_TIME = 30.0f;
   // private int intTime = 0;

    private int intScore = 0;
    private int perfectCount = 0;
    private int greatCount = 0;
    private int goodCount = 0;
    // Start is called before the first frame update
    void Start()
    {
        ScoreUI.text = "Score: " + intScore;
        intScore = 0;
        perfectCount = 0;
        greatCount = 0;
        goodCount = 0;
        PlayerPrefs.SetInt("score", intScore);
        PlayerPrefs.SetInt("perfect", perfectCount);
        PlayerPrefs.SetInt("great", greatCount);
        PlayerPrefs.SetInt("good", goodCount);
    }

    // Update is called once per frame
    void Update()
    {
        int paticleCount = listParticle.Count;
        if (paticleCount > 0)
        {
            for (int i = 0; i < paticleCount; i++)
            {
                if (!listParticle[i].IsAlive())
                {
                    listParticle[i].gameObject.Kill();
                    listParticle.Remove(listParticle[i]);
                    paticleCount--;
                }
            }
        }
    }
    public void SetTimer(float timer)
    {
        Timer.GetComponent<TimerBar>().SetCurrentTime(timer);
    }
    public void SetTimeMax(float timeMax)
    {
        Timer.GetComponent<TimerBar>().SetMaxTime(timeMax);
    }
    public void IncrementScore(int score)
    {
        intScore += score;
        ScoreUI.text = "Score: " + intScore;
        PlayerPrefs.SetInt("score", intScore);
        if (score == 100)
        {
            perfectCount++;
            PlayerPrefs.SetInt("perfect", perfectCount);
            GameObject temp = perfect.Spawn(new Vector3(0.0f, 0.0f, 0.0f), Quaternion.Euler(-90.0f, 0f, 0f), Vector3.one);
            temp.GetComponent<ParticleSystem>().Play();
            listParticle.Add(temp.GetComponent<ParticleSystem>());
        }
        else if (score == 80)
        {
            greatCount++;
            PlayerPrefs.SetInt("great", greatCount);
            GameObject temp = great.Spawn(new Vector3(0.0f, 0.0f, 0.0f), Quaternion.Euler(-90.0f, 0f, 0f), Vector3.one);
            temp.GetComponent<ParticleSystem>().Play();
            listParticle.Add(temp.GetComponent<ParticleSystem>());
        }
        else if (score == 50)
        {
            goodCount++;
            PlayerPrefs.SetInt("good", goodCount);
            GameObject temp = good.Spawn(new Vector3(0.0f, 0.0f, 0.0f), Quaternion.Euler(-90.0f, 0f, 0f), Vector3.one);
            temp.GetComponent<ParticleSystem>().Play();
            listParticle.Add(temp.GetComponent<ParticleSystem>());
        }
    }
}
