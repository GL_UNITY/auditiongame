﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using System;
using UnityEngine.Events;

[System.Serializable]
public class AudioEvent : UnityEvent<float>
{
    // https://docs.unity3d.com/ScriptReference/Events.UnityEvent_1.html
}

public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;
    public AudioEvent audioEvent;
    public AudioEvent audioMaxTimeEvent;
    public AudioEvent audioEndEvent;
    private string currentName;

    // Start is called before the first frame update
    void Awake()
    {
        foreach(Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
        }

        currentName = "";
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
            OnSoundEnd(-1);
        if (currentName.Length > 0)
        {
            //Debug.Log(GetLength("Music"));
            float timeMusic = GetSound(currentName).source.time;
            float lengthMusic = GetSound(currentName).clip.length;
            //Debug.Log(timeMusic + " " + lengthMusic);
            if (timeMusic >= lengthMusic)
            {
                OnSoundEnd(-1f);
            }
            else
            {
                OnSoundRun(timeMusic);
            }
        }
    }

    private void OnSoundEnd(float time)
    {
        audioEndEvent.Invoke(time);
    }

    private void OnSoundRun(float time)
    {
        audioEvent.Invoke(time);
    }

    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if(s == null)
        {
            return;
        }
        s.source.Play();
        currentName = name;

        audioMaxTimeEvent.Invoke(s.clip.length);
    }

    public float GetTime(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if(s == null)
        {
            return -1f;
        }
        return s.source.time;        
    }

    public float GetLength(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if(s == null)
        {
            return -1f;
        }
        return s.clip.length;        
    }

    public bool IsPlaying(int index)
    {
        return sounds[index].source.isPlaying;
    }

    public Sound GetSound(string name)
    {
        return Array.Find(sounds, sound => sound.name == name);
    }
}
