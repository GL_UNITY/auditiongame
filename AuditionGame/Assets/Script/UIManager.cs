﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class UIEvent : UnityEvent
{
    // https://docs.unity3d.com/ScriptReference/Events.UnityEvent_1.html
}

public class UIManager : MonoBehaviour
{
    public UIEvent uiEvent;
    public GameObject introSplash;
    public GameObject start321;
    private Animator anim321;
    private bool isFinishAnim321;
    // Start is called before the first frame update
    void Start()
    {
        anim321 = start321.GetComponent<Animator>();
        isFinishAnim321 = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (anim321.GetCurrentAnimatorStateInfo(0).IsName("Start") && anim321.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !isFinishAnim321)
        {
            isFinishAnim321 = true;
            StartGame();
        }
    }

    public void DisableIntroSplash()
    {
        introSplash.SetActive(false);
    }

    public void Start321()
    {
        start321.SetActive(true);
    }

    public void StartGame()
    {
        uiEvent.Invoke();
    }
    
}
